const webpack = require("webpack");
const path = require("path");

let config = {
    entry: ["./src/index.js",
    "./src/filter.js",
    "./src/data.js",
    "./src/labels.js",
    "./src/lists.js",
    "./src/recipes.js",
    "./src/search.js",
    "./src/utils.js",
],
    output: {
      path: path.resolve(__dirname, "./public"),
      filename: "./bundle.js",
    }
}
  
module.exports = config;
