import { createAllRecipes } from "./recipes";
import { displayLists } from "./filter";
import { manageSearchInput } from "./search";
import { fillLists} from "./lists"

fillLists();
// Fill the lists arrays
displayLists();
// Display all the recipes 
//createAllRecipes();
// Select and add an event to the search bar
const searchBarElt = document.getElementById("search-bar");
searchBarElt.addEventListener("input", manageSearchInput);