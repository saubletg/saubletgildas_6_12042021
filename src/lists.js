import { data } from "./data";

// Create a lists object with all types of lists (ing, app, ust, ids)
let lists = {
  ingList: [],
  appList: [],
  ustList: [],
  recipesIds: [],
  fullRecipes: [],
  ingLabels: [],
  appLabels: [],
  ustLabels: [],
  ingObject: {},
  appObject: {},
  ustObject: {},
  currentSearch: "",
};
  
// List of all Ingredients
const allIngList = () => {
    let ingList = [];
    data.forEach(recipe => {
      recipe.ingredients.forEach(ing => {
        ingList.push(ing.ingredient.toLowerCase().replace(/[.]/g, ""))
      })
    })
    ingList = new Set(ingList);
    ingList = [...ingList].sort();
    return ingList.map((ing) => ({
      type: "ing",
      name: ing,
    }));
};
  
// List of all Appliances
const allAppList = () => {
    let appList = [];
    data.forEach(recipe => {
      appList.push(recipe.appliance.toLowerCase().replace(/[.]/g, ""))
      })
    appList = new Set(appList)
    appList = [...appList].sort();
    return appList.map((app) => ({
      type: "app",
      name: app,
    }));
};
  
// List of all Ustensils
const allUstList = () => {
    let ustList = [];
    data.forEach(recipe => {
        recipe.ustensils.forEach(ust => {
          ustList.push(ust.toLowerCase().replace(/[.]/g, ""))
        })
      })
    ustList = new Set(ustList);
    ustList = [...ustList].sort();
    return ustList.map((ust) => ({
      type: "ust",
      name: ust,
    }));
};

// List of all Recipes Id
const allRecipesIds = () => {
    return data.map((elt) => elt.id);
};

// Create an object with all tags of a types
 const createTagObject = (tagList) => {
  let tagObj = {};
  tagList.forEach(tag => tagObj[tag.name] = []);
  return tagObj;
};

//Create an object with all ingredient names
const getIngredientsObject = () => {
  let ingredientsObject = createTagObject(allIngList());
  data.forEach((recipe) => {
    recipe.ingredients.forEach((elt) => {
      ingredientsObject[elt.ingredient.toLowerCase().replace(/[.]/g, "")].push(recipe.id);
    });
  });
  return ingredientsObject;
};

// Create an object with all appliance names
const getAppliancesObject = () => {
  let appliancesObject = createTagObject(allAppList());

  data.forEach((recipe) => {
    appliancesObject[recipe.appliance.toLowerCase().replace(/[.]/g, "")].push(recipe.id);
  });

  return appliancesObject;
};

// Create an object with all ustensil names
const getUstensilsObject = () => {
  let ustensilsObject = createTagObject(allUstList());

  data.forEach((recipe) => {
    recipe.ustensils.forEach((elt) => {
      ustensilsObject[elt.toLowerCase().replace(/[.]/g, "")].push(recipe.id);
    });
  });
  return ustensilsObject;
};

// Get a unique string with all ingredients from one recipe
const ingredientsStringFromRecipe = (recipe) => {
  let ingredientsString = "";
  recipe.ingredients.forEach((ing) => {
    ingredientsString += ` ${ing.ingredient.toLowerCase()}`;
  });
  return ingredientsString;
};

//Get full recipe data from Ids
const fullRecipesFromIds = (idsArray) => {
  return data.filter((recipe) => idsArray.includes(recipe.id));
};

// Fill the lists arrays
const fillLists = () => {
  lists.ingList = allIngList();
  lists.appList = allAppList();
  lists.ustList = allUstList();
  lists.recipesIds = allRecipesIds();
  lists.ingObject = getIngredientsObject();
  lists.appObject = getAppliancesObject();
  lists.ustObject = getUstensilsObject();
};

export { 
  lists, 
  fillLists, 
  ingredientsStringFromRecipe, 
  fullRecipesFromIds,
  allRecipesIds };