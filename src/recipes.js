import { data } from "./data";
import { createEltWithClassName, createLinkElt } from "./utils";

// Select DOM element
const mainContentElt = document.getElementById("main-content");

// Create single recipe
const createRecipeElement = (recipe) => {
  const elt = createEltWithClassName("article", "recipe");
  elt.id = recipe.id;
  const aElt = createLinkElt("index.html", recipe.name);
  aElt.textContent = "";
  const imgElt = createEltWithClassName("div", "recipe-img");
  const dataElt = createEltWithClassName("div", "recipe-data");
  const headerElt = createEltWithClassName("header");
  const h2Elt = createEltWithClassName("h2");
  const timeElt = createEltWithClassName("div", "recipe-time");
  const categoryLeftElt = createEltWithClassName("div", "recipe-category");
  const categoryRightElt = createEltWithClassName("div", "recipe-category");

  const ulElt = createEltWithClassName("ul");
  recipe.ingredients.forEach((ingredient) => {
    const liElt = createIngredient(ingredient);
    ulElt.appendChild(liElt);
  });

  h2Elt.textContent = recipe.name;
  timeElt.innerHTML = `<span class="far fa-clock"></span> ${recipe.time} min`;
  categoryRightElt.textContent = recipe.description;

  headerElt.appendChild(h2Elt);
  headerElt.appendChild(timeElt);
  categoryLeftElt.appendChild(ulElt);
  dataElt.appendChild(headerElt);
  dataElt.appendChild(categoryLeftElt);
  dataElt.appendChild(categoryRightElt);
  aElt.appendChild(imgElt);
  aElt.appendChild(dataElt);
  elt.appendChild(aElt);
  elt.style.display = "block";

  return elt;
};

// Create single ingredient 
const createIngredient = (ingredient) => {
  const liElt = document.createElement("li");
  let template = `<strong>${ingredient.ingredient} </strong>`;
  (ingredient.quantity != null) ? template += ` <span>${ingredient.quantity}</span>` : null ;
  if (ingredient.unit){
    template += ` <span>${ingredient.unit}</span>`
  }
  liElt.innerHTML = template;

  return liElt;
};

// Create all recipes and insert them into the DOM
const createAllRecipes = () => {
  mainContentElt.innerHTML = "";
  data.forEach((recipe) => {
    const recipeElt = createRecipeElement(recipe);
    recipeElt.style.display = "block";
    mainContentElt.appendChild(recipeElt);
  });
};

//Display all the recipes in the DOM
const displayAllRecipes = () => {
  data.forEach((recipe) => {
    const recipeToDisplay = document.getElementById(recipe.id);
    recipeToDisplay.style.display = "block";
  });
};

//Check if recipe is already in the DOM and remove it
const removeRecipeById = (recipeId) => {
  if (document.getElementById(recipeId))
    document.getElementById(recipeId).remove();
};

export { createAllRecipes, 
  removeRecipeById, 
  createRecipeElement,
  displayAllRecipes };