// Creates a dom element with a class
const createEltWithClassName = (eltTag, eltClass, eltClass2) => {
    const elt = document.createElement(eltTag);
    elt.classList.add(eltClass);
    elt.classList.add(eltClass2);
  
    return elt;
};
  
// Creates a link element with a class
const createLinkElt = (eltHref, eltContent, eltClass) => {
    const elt = createEltWithClassName("a", eltClass);
    elt.setAttribute("href", eltHref);
    elt.setAttribute("title", eltContent);
    elt.textContent = eltContent;
    return elt;
};
  
export { createEltWithClassName, createLinkElt };