import { data } from "./data";
import { 
  lists,
  fullRecipesFromIds,
  allRecipesIds, 
} from "./lists";
import {
  createRecipeElement,
  removeRecipeById,
  createAllRecipes,
  } from "./recipes";
import { 
  labelsLength,
  clearAllFilters,
  getVisibleFilters,
  createFilterElt 
} from "./filter";

// Create a search object
let search = {
  curentSearch: ""
};

// Manage the conditions
const manageSearchInput = (evt) => {
  evt.preventDefault();
  search.currentSearch = evt.target.value;
  displaySearch();
};

// Search by input logic
const searchByInput = () => {
  const value = search.currentSearch;
  
  if (value && value.length > 2) {
    createAllRecipes();
    data.forEach((recipe) => {
      
      const displayedRecipe = document.getElementById(recipe.id);
      console.log(displayedRecipe)
      if 
      (recipe.name.toLowerCase().includes(value.toLowerCase())== false) 
      {
        if
        (recipe.description.toLowerCase().includes(value.toLowerCase())== false)
        {
          displayedRecipe.style.display = "none";
        }
      }
      else {
        displayedRecipe.style.display = "block";
      }
    });
  }  
};
/*const searchByInput = () => {
  const mainContentElt = document.getElementById("main-content");
  const value = search.currentSearch;
  lists.recipesIds = [];
  
  if (value && value.length > 2) {
    // Search by name
    data.forEach((recipe) => {
      if ((recipe.name.toLowerCase()).includes(value.toLowerCase())||
      (recipe.description.toLowerCase()).includes(value.toLowerCase())
      ) {
        removeRecipeById(recipe.id);
        mainContentElt.appendChild(createRecipeElement(recipe));
        lists.recipesIds.push(recipe.id);
      } else {
        removeRecipeById(recipe.id);
      }
    });
  } else {
    mainContentElt.innerHTML = "";
  }
};*/

// Search by tags logic
const searchByTag = () => {
  const {
    ingLabels,
    appLabels,
    ustLabels,
    recipesIds,
    ingObject,
    appObject,
    ustObject,
    currentSearch,
  } = lists;

  let arrayOfRecipes = recipesIds;

  if (currentSearch.length < 3 
    && labelsLength() && labelsLength() > 0)
  { 
    arrayOfRecipes = allRecipesIds();
    createAllRecipes();
  }
  arrayOfRecipes.forEach((id) => {
    const displayedRecipe = document.getElementById(id);

    ingLabels.forEach((ing) => {
      if (!ingObject[ing].includes(id)){
        displayedRecipe.style.display = "none";
      }
    });

    appLabels.forEach((app) => {
      if (!appObject[app].includes(id)){
        displayedRecipe.style.display = "none";
      }
    });

    ustLabels.forEach((ust) => {
      if (!ustObject[ust].includes(id)){
        displayedRecipe.style.display = "none";
      }
    });
  });
};

// Display the filters from the displayed recipes
const displayRemainingTags = () => {
  let recipesToConsider = [];

  if (search.currentSearch && search.currentSearch.length < 3 
    && labelsLength() && labelsLength() === 0) {
    recipesToConsider = data;
  } else {
    const allRecipes = document.querySelectorAll("#main-content article");
    const visibleRecipesIds = Array.from(allRecipes)
      .filter((elt) => elt.style.display === "block")
      .map((elt) => parseInt(elt.id));
    recipesToConsider = fullRecipesFromIds(visibleRecipesIds);
  }

  clearAllFilters();
  recipesToConsider.forEach((recipe) => displayFiltersFromRecipes(recipe));
};

// Display all filters included in the displayed recipes
const displayFiltersFromRecipes = (recipe) => {
  displayIngredientsFromRecipe(recipe);
  displayAppliancesFromRecipe(recipe);
  displayUstensilsFromRecipe(recipe);
};

// Display the ingredient filters included in the displayed recipes
const displayIngredientsFromRecipe = (recipe) => {
  const ingListElt = document.getElementById("ing-filter-list");
  const visibleIngFilters = getVisibleFilters("ing");

  recipe.ingredients.forEach((ing) => {
    if (!visibleIngFilters.includes(ing.ingredient.toLowerCase()))
      ingListElt.appendChild(
        createFilterElt("ing", ing.ingredient.toLowerCase())
      );
  });
};

// Display the appliance filters included in the displayed recipes
const displayAppliancesFromRecipe = (recipe) => {
  const appListElt = document.getElementById("app-filter-list");
  const visibleAppFilters = getVisibleFilters("app");

  if (!visibleAppFilters.includes(recipe.appliance.toLowerCase()))
    appListElt.appendChild(
      createFilterElt("app", recipe.appliance.toLowerCase())
    );
};

// Display the ustensil filters included in the displayed recipes
const displayUstensilsFromRecipe = (recipe) => {
  const ustListElt = document.getElementById("ust-filter-list");
  const visibleUstFilters = getVisibleFilters("ust");

  recipe.ustensils.forEach((ust) => {
    if (!visibleUstFilters.includes(ust.toLowerCase()))
      ustListElt.appendChild(createFilterElt("ust", ust.toLowerCase()));
  });
};

// Display "no result" when no recipe displayed
const checkSearchResults = () => {
 const allRecipes = document.querySelectorAll("#main-content article");
 const mainContentElt = document.getElementById("result");

 const hiddenRecipes = Array.from(allRecipes).filter(
   (elt) => elt.style.display === "none"
 );

 if (allRecipes.length === 0 || hiddenRecipes.length === allRecipes.length) {
   if (search.currentSearch && search.currentSearch.length < 3 
    && labelsLength() && labelsLength() === 0) {
     mainContentElt.textContent = "";
   } else {
     mainContentElt.textContent =
       "Aucune recette ne correspond à votre critère…";
   }
 } else {
   mainContentElt.textContent = "";
 }
};

// Display the complet search
const displaySearch = () => {
  searchByInput();
  searchByTag();
  displayRemainingTags();
  checkSearchResults();
};



export { manageSearchInput, displaySearch, searchByTag };