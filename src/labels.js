import { createEltWithClassName } from "./utils";
import { lists } from "./lists"

// Select the DOM element
const labelsElt = document.getElementById("labels");

// Creates a single generic label element (ing, app, ust & label name)
const createLabel = (type, name) => {
  const elt = createEltWithClassName("button","label",`${type}`);
  elt.setAttribute("type", "button");
  const iconElt = createEltWithClassName("span", "far", "fa-times-circle");
  iconElt.addEventListener("click", removeFilter(type, name));
  elt.textContent = name;
  elt.appendChild(iconElt);

  return elt;
};

// Create all the labels of a type (ing, app, ust) in a label list
const createLabels = (labelsList, type) => {
  const elt = document.createElement("div");
  labelsList.forEach((label) => {
    const labelElt = createLabel(type, label);
    elt.appendChild(labelElt);
  });
  return elt;
};

// Create the labels lists of all types(ing/app/ust)
const createAllLabels = () => {
  labelsElt.innerHTML = "";
  labelsElt.appendChild(createLabels(lists.ingLabels, "ing"));
  labelsElt.appendChild(createLabels(lists.appLabels, "app"));
  labelsElt.appendChild(createLabels(lists.ustLabels, "ust"));
};

// Remove a label
const removeFilter = (type, name) => {
  return function (evt) {
    evt.preventDefault();  
    if (type === "ing")
        lists.ingLabels = lists.ingLabels.filter((elt) => name !== elt);
    if (type === "app")
        lists.appLabels = lists.appLabels.filter((elt) => name !== elt);
    if (type === "ust")
        lists.ustLabels = lists.ustLabels.filter((elt) => name !== elt);
    createAllLabels();
  };
};

export { createAllLabels };