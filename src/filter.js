import { createEltWithClassName, createLinkElt } from "./utils";
import { lists } from "./lists";
import { createAllLabels } from "./labels";
import { displaySearch, searchByTag } from "./search";


// Select the DOM elements 
// All the filters lists (ing, app, ust)
const ingFiltersListElt = document.getElementById("ing-filter-list");
const appFiltersListElt = document.getElementById("app-filter-list");
const ustFiltersListElt = document.getElementById("ust-filter-list");
// All the filters buttons (ing, app, ust)
const ingBtnElt = document.getElementById("ing-btn");
const appBtnElt = document.getElementById("app-btn");
const ustBtnElt = document.getElementById("ust-btn");
// All the filters input (ing, app, ust)
const ingInputElt = document.getElementById("ing-input");
const appInputElt = document.getElementById("app-input");
const ustInputElt = document.getElementById("ust-input");

// Creates generics elements
// Creates a "li" & "a" element with a name
const createFilterElt = (type, name) => {
  const liElt = createEltWithClassName("li");
  const aElt = createLinkElt("index.html", name, "filter-tag");
  liElt.appendChild(aElt);
  liElt.addEventListener("click", addLabel(type, name));

  return liElt;
};

// Add a label onClick and push it into an array
 const addLabel = (type, name) => {
  return function (evt) {
    evt.preventDefault();
    
    if (type === "ing") {
      if (!lists.ingLabels.includes(name.toLowerCase()))
        lists.ingLabels.push(name.toLowerCase());
    }
    if (type === "app") {
      if (!lists.appLabels.includes(name.toLowerCase()))
      lists.appLabels.push(name.toLowerCase());
    }
    if (type === "ust") {
      if (!lists.ustLabels.includes(name.toLowerCase()))
        lists.ustLabels.push(name.toLowerCase());
    }
    putFiltersToInitialState();
    createAllLabels();
    displaySearch();
  };
};

//Get the number of labels added (ing + app + ust)
const labelsLength = () => {
  const fullTagArray = lists.ingLabels
    .concat(lists.appLabels)
    .concat(lists.ustLabels);
  return fullTagArray.length; 
};

// Creates a list element with all filters elements
const createFiltersList = (listElt, list) => {
  listElt.innerHTML = "";
  list.forEach((elt) => {
    const liElt = createFilterElt(elt.type, elt.name);
    listElt.appendChild(liElt);
  });
};

// Display the differents filters lists on click
const displayLists = () => {
  fillAllFilterLists();
  ingBtnElt.addEventListener("click", manageEvents);
  appBtnElt.addEventListener("click", manageEvents);
  ustBtnElt.addEventListener("click", manageEvents);
  ingInputElt.addEventListener("focus", scaleFilterUp);
  appInputElt.addEventListener("focus", scaleFilterUp);
  ustInputElt.addEventListener("focus", scaleFilterUp);
  ingInputElt.addEventListener("input", manageCompletion);
  appInputElt.addEventListener("input", manageCompletion);
  ustInputElt.addEventListener("input", manageCompletion);
};

// Fills the differents lists with data from lists object
const fillAllFilterLists = () => {
  createFiltersList(ingFiltersListElt, lists.ingList);
  createFiltersList(appFiltersListElt, lists.appList);
  createFiltersList(ustFiltersListElt, lists.ustList);
};

// Manage the events on click
const manageEvents = (evt) => {
  evt.preventDefault();
  if (evt.target.tagName !== "SPAN") return;

  if (evt.target.className === "fas fa-chevron-down") {
    scaleFilterUp(evt);
  } else {
    closeAllFilterLists();
    scaleAllFiltersDown();
  }
};

// Event when the Btn down is clicked (open)
// Scale the input filter up
const scaleFilterUp = (evt) => {
  evt.preventDefault();
  scaleAllFiltersDown();
  const parentElt = evt.target.parentNode;
  parentElt.className += " scaled";
  openFilterList(parentElt.childNodes[5], parentElt.childNodes[3].id);
};

// Open the filter list with associated button type
const openFilterList = (elt, btn) => {
  closeAllFilterLists();
  elt.className += " open";
  const buttonElt = document.getElementById(btn);
  buttonElt.className = "fas fa-chevron-up";
};

// Events that closed the filters lists (close)
// Close all filters
const closeAllFilterLists = () => {
  ingFiltersListElt.parentNode.classList.remove("open");
  appFiltersListElt.parentNode.classList.remove("open");
  ustFiltersListElt.parentNode.classList.remove("open");

  ingBtnElt.className = "fas fa-chevron-down";
  appBtnElt.className = "fas fa-chevron-down";
  ustBtnElt.className = "fas fa-chevron-down";

  removeFilterInputsValue();
  resetTagsDisplay();
};

// Scale the input filter down
const scaleAllFiltersDown = () => {
  const filtersElts = document.querySelectorAll(".filters-elt");
  filtersElts.forEach((elt) => elt.classList.remove("scaled"));
};

// Clear the inputs
// Clear value in filters inputs
const removeFilterInputsValue = () => {
  ingInputElt.value = "";
  appInputElt.value = "";
  ustInputElt.value = "";
};

//close all filters and scale them down
 const putFiltersToInitialState = () => {
  closeAllFilterLists();
  scaleAllFiltersDown();
};

// Clear the DOM filters
const clearAllFilters = () => {
  ingFiltersListElt.innerHTML = "";
  appFiltersListElt.innerHTML = "";
  ustFiltersListElt.innerHTML = "";
};

//Get the list of currently displayed filters
const getVisibleFilters = (type) => {
  const listNodes = document.querySelectorAll(`#${type}-filter-list li a`);
  const listContentArray = Array.from(listNodes).map((elt) =>
    elt.textContent.toLowerCase()
  );
  return listContentArray;
};

//autocompletion when typing in filter's inputs
const manageCompletion = (evt) => {
  const idTarget = evt.target.parentNode.id;
  const formattedValue = evt.target.value.toLowerCase();
  const allTags = document.querySelectorAll(`#${idTarget} li a`);

  allTags.forEach((tag) => {
    if (!tag.textContent.includes(formattedValue))
      tag.parentNode.style.display = "none";
    else tag.parentNode.style.display = "block";
  });
};

// Display all tags in the DOM
const resetTagsDisplay = () => {
  const allIngTags = document.querySelectorAll(`#ing-filter-list li`);
  const allAppTags = document.querySelectorAll(`#app-filter-list li`);
  const allUstTags = document.querySelectorAll(`#ust-filter-list li`);

  allIngTags.forEach((ing) => (ing.style.display = "block"));
  allAppTags.forEach((app) => (app.style.display = "block"));
  allUstTags.forEach((ust) => (ust.style.display = "block"));
};

export { displayLists, labelsLength, clearAllFilters, getVisibleFilters, createFilterElt };